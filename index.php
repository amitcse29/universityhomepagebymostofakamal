<?php
session_start();
if($_SESSION['name'] == 'sessionclass'){
?>
<!DOCTYPE html>
<html>
<head>
<title>Stanford University</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Stanford University is one of the world's leading research and teaching institutions.  It is located in Stanford, California." />
<meta name="author" content="Stanford Office of University Commmunications" />

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="main.css">


</head>
<body>

<header>
	<div class="row am-header">
		<div class="container am-container-margin">


		  <div class="col-md-4">
			
			<img src="img/stanforduniversity-white@2x.png" width="360" height="46" />
		  </div>


		  <div class="col-md-4  col-md-offset-4 am-header-width">
			<label for="search-web">Web</label>
			<input id="search-web" class="radio-search"  type="radio" value="web" name="search_type"></input>
			<label for="search-web">People</label>
			<input id="search-web" class="radio-search" type="radio" value="web" name="search_type"></input>
			<input class="input-medium search-field" placeholder="Search web or people" name="search_string" value="" size="15" maxlength="128" type="text">

          </div>

        </div>

	</div>
	
		
		
	
</header>

<nav>
	<div class="row am-nav-bg">
		
		<div class="container">
			<div class="col-md-8">
				<ul class="am-nav">
					<li><a href="">About Stanford</a></li>
					<li><a href="">Admission</a></li>
					<li><a href="">Academics </a></li>
					<li><a href="">Research</a></li>
					<li><a href="">Campus Life</a></li>
					
				</ul>
			</div>
			
			<div class="col-md-4">
				<ul class="am-nav">
					<li><a href="">Students</a></li>
					<li><a href="">Faculty/Staff</a></li>
					<li><a href="">Parents</a></li>
					
					<li><a href="logout.php">Logout</a></li>
				</ul>
			</div>
		</div>
			
			
	</div>


</nav>

<section class="margin-top">
	<div class="row">
		<div class="container">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			  

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
				<div class="item active">
				  <img src="img/1.jpg" alt="...">
				</div>
				<div class="item">
				  <img src="img/2.jpg" alt="...">
				  
				</div>
				<div class="item">
				  <img src="img/3.jpg" alt="...">
				  
				</div>
				<div class="item">
				  <img src="img/4.jpg" alt="...">
				  
				</div>
				<div class="item">
				  <img src="img/5.jpg" alt="...">
				  
				</div>
				
			  </div>
				
			<!-- Indicators -->
			  <ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				<li data-target="#carousel-example-generic" data-slide-to="3"></li>
				<li data-target="#carousel-example-generic" data-slide-to="4"></li>
				
			  </ol>
			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			  </a>
			  
			  
			</div>
			
		</div>
	</div>

</section>

<section class="margin-top-md">
	
	<div class="row">
		<div class="container">
			<h4 style="border-bottom:1px solid #C0C0C0;color:#C1C1C1;padding:2px;line-height:29px">Top Stories</h4>
			<div class="col-md-3">
				<img src="img/s1.jpg" alt="" />
				<h3><a href="">Making waves</a></h3>
				<p>Stanford scholar illuminates history of disputed China Sea islands.</p>
				
			
			</div>
			
			<div class="col-md-3">
				<img src="img/s2.jpg" alt="" />
				<h3><a href="">Making waves</a></h3>
				<p>Stanford scholar illuminates history of disputed China Sea islands.</p>
			
			</div>
			
			<div class="col-md-3">
				<img src="img/s3.jpg" alt="" />
				<h3><a href="">Making waves</a></h3>
				<p>Stanford scholar illuminates history of disputed China Sea islands.</p>
			
			</div>
			
			<div class="col-md-3">
				<h4><a href="">MORE HEADLINES</a></h4>
				<p><i class="fa fa-chevron-circle-right"></i> Health care law's effect on dental care</p>
				<p><i class="fa fa-chevron-circle-right"></i> What really matters to early-stage investors</p>
				<p><i class="fa fa-chevron-circle-right"></i> School of Medicine researchers receive $14 million to match genetics to drug responses</p>
				<p class="more-link">
					<a class="su-link"  href="">
						<i class="fa fa-chevron-circle-right"></i>
						<span>More News</span>
					</a>
				</p>
			
			</div>
			
		</div>
	</div>
</section>
<section>
	
	<div class="container">
		<div class="row">
			<div class="col-md-9 well"> 
				<!-- Social Feature include file --> 
				
					<time datetime="2015-08-06T14:42:02-07:00">9 hr.</time>ago&nbsp;&nbsp;
					  <a href="" style="background:#fff;padding:8px;border-radius:3px;color:#666;line-height:35px" id="badge">
						<i class="fa fa-twitter" style="color:#00beeb"></i> <span >@Stanford</span>
					  </a> 
					
					
					&nbsp;&nbsp;Shallow fracking  drinking water sources, according to a new analysis led by Prof. Rob Jackson: <a data-ua-action="url" class="tweet-link" href="">stanford.io/1DxjUcv</a>
			

			</div> 
			<div class="col-md-3">
				<i class="fa fa-facebook social-icons social-color-fb"></i>
				<i class="fa fa-twitter social-icons social-color-twitter"></i>
				<i class="fa fa-apple social-icons social-color-google"></i>
				<i class="fa fa-youtube social-icons social-color-youtibe"></i>
				<i class="fa fa-instagram social-icons social-color-instagram"></i>
				

				
		  
		  </div>
          </div>
		  
		  
		</div>
	</div>

</section>

<section>
	
	<div class="container">
		
		<div class="row">
		
		
			<div class="col-md-4">
				<h4 style="border-bottom:1px solid #C0C0C0;color:#C1C1C1;padding:2px;line-height:29px">At Stanford</h4>
				
					<img  class="img-responsive" src="img/anderson_collection.jpg" alt="" />
					<div class="image-caption">
					<p style="background:#F2F1EB;padding:10px;"><a href=""> DH.SCHOOL </a>One of the world's most outstanding collections of modern and contemporary American art.</p>
					</div>
				
				
				
			</div>
			
			<div class="col-md-4">
				<h4 style="border-bottom:1px solid #C0C0C0;color:#C1C1C1;padding:2px;line-height:29px">Events</h4>
				
				<div class="event">
					<div class="date float-left">
					<span class="month">AUG</span> <br><span class="day"><a href="">6</a></span>
					</div>
					<div class="text float-right">
					<p>Stanford Jazz Workshop Showcase</p>
					<p class="timestamps">7:00 p.m.</p>
					</div>
				</div>
				
				<div class="event">
					<div class="date float-left">
					<span class="month">AUG</span> <br><span class="day"><a href="">8</a></span>
					</div>
					<div class="text float-right">
					<p>Stanford Jazz Workshop Showcase</p>
					<p class="timestamps">7:00 p.m.</p>
					</div>
				</div>
				
				<div class="event">
					<div class="date float-left">
					<span class="month">AUG</span> <br><span class="day"><a href="">10</a></span>
					</div>
					<div class="text float-right">
					<p>Stanford Jazz Workshop Showcase</p>
					<p class="timestamps">7:00 p.m.</p>
					</div>
				</div>
				
				
				<p style="clear:both"class="float-right">
					<a class="su-link"  href="">
						<i class="fa fa-chevron-circle-right"></i>
						<span>Event Calendar</span>
					</a>
				</p>
				
				
			</div>
			
			<div class="col-md-4">
				<h4 style="border-bottom:1px solid #C0C0C0;color:#C1C1C1;padding:2px;line-height:29px"> Athletics </h4>
				<div class="event">
					<div class="date float-left">
					<img src="img/athletics.png" alt="" />
					</div>
					<div class="text float-right">
					<h5 style="font-size:17px">Stanford Cardinal</h5>
					<p>Stanford Jazz Workshop Showcase</p>
					<p class="timestamps"></p>
					
					<p>
					<a class="su-link"  href="">
						<i class="fa fa-chevron-circle-right"></i>
						<span>GoStanford.com</span>
					</a>
					</p>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>

</section>

<section>
	
	<div class="row well am-well">
		<div class="container">
		<div class="col-md-2">
			<div class="amlinks">
			  <a style="color:#8C1515;text-transform:uppercase;font-size:14px" href="">Schools</a><br>
			  
				<a href="">Business</a><br>
				<a href="">Earth, Energy & Environmental Sciences</a><br>
				<a href="">Education</a><br>
				<a href="">Engineering</a><br>
				<a href="">Humanities & Sciences</a><br>
				<a href="">Law</a><br>
				<a href="">Medicine</a><br>
				
			
			</div>
		</div>
		<div class="col-md-2">
			
			<div class="amlinks">
			  <a style="color:#8C1515;text-transform:uppercase;font-size:14px" href="">Departments</a><br>
			  
				<a href="">Departments A - Z</a><br>
				<a href="">Interdisciplinary Programs</a><br>
			
			</div>
			<br>
			<div class="amlinks">
			  <a style="color:#8C1515;text-transform:uppercase;font-size:14px" href="">Research</a><br>
			  
				<a href="">DoResearch</a><br>
				<a href="">Interdisciplinary Institutes</a><br>
				<a href="">Libraries</a><br>
			
			</div>
			
			
		</div>
		<div class="col-md-2">
			<div class="amlinks">
			  <a style="color:#8C1515;text-transform:uppercase;font-size:14px" href="">Health care</a><br>
			  
				<a href="">Stanford Health Care</a><br>
				<a href="">Stanford Children's Health</a><br>
			
			</div>
			<br>
			<div class="amlinks">
			  <a style="color:#8C1515;text-transform:uppercase;font-size:14px" href="">Online Learning</a><br>
			  
				<a href="">Stanford Online</a><br>
				
			
			</div>
		
		</div>
		<div class="col-md-2">
			
			<div class="amlinks">
			  <a style="color:#8C1515;text-transform:uppercase;font-size:14px" href="">About Stanford</a><br>
			  
				<a href="">Facts</a><br>
				<a href="">History</a><br>
				<a href="">Accreditation</a><br>
			
			</div>
			<br>
			<div class="amlinks">
			  <a style="color:#8C1515;text-transform:uppercase;font-size:14px" href="">Admission</a><br>
			  
				<a href="">Undergraduate</a><br>
				<a href="">Graduate</a><br>
				<a href="">Financial Aid</a><br>
				
			
			</div>
		
		
		</div>
		<div class="col-md-2">
			<div class="amlinks">
			  <a style="color:#8C1515;text-transform:uppercase;font-size:14px" href="">Resources</a><br>
			  
				<a href="">A - Z Index</a><br>
				<a href="">Campus Map</a><br>
				<a href="">Directory</a><br>
				<a href="">Stanford Profiles</a><br>
			
			</div>
		
		</div>
		<div class="col-md-2">
		
			
		<div class="am-btn">
         <a  href="/admission"><i class="fa fa-graduation-cap fa-fw"></i> <span>Apply</span> </a> 
          <a  href=""><i class="fa fa-plane fa-fw"></i> <span>Visit Campus</span></a>
          <a data-ua-label="footer" href=""><i class="fa fa-gift fa-fw"></i> <span>Make a Gift</span></a>
		  
		<a  href=""><i class="fa fa-user fa-fw"></i> <span>Find a Job</span> </a>
         <a  href="/contact"><i class="fa fa-comment-o fa-fw"></i> <span>Contact Us</span></a>
       </div>
		
		
		</div>
	
	</div>
	</div>


</section>

<footer>
	<div class="row footer-bg">
		<div class="container">
			
			<div class="footer-main">
			
			
			
				<div class="footer-content float-left">
					<img src="img/footer-stanford-logo@2x.png" alt="" />
					
				
				</div>
				<div class="footer-links float-left">
					<a href="">SU Home</a><a href="">SU Home</a><a href="">Maps & Directions</a><a href=""> Search Stanford</a>
					<a href="">Terms of Use</a><a href="">Emergency Info</a>
					
					<p>&copy; Stanford University.  Stanford, California 94305. Copyright Complaints   Trademark Notice</p>
				
				</div>
			
			</div>
		
		</div>
	</div>

</footer>


<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html> 
<?php
}
else{
    header('Location:login.php');
}
?>
